# libwnck 
# Young-Ho, Cha <ganadist@chollian.net>, 2002.
# Changwoo Ryu <cwryu@debian.org>, 2002-2005, 2007, 2008, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: libwnck\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2009-01-29 16:38+0900\n"
"PO-Revision-Date: 2009-01-29 16:39+0900\n"
"Last-Translator: Changwoo Ryu <cwryu@debian.org>\n"
"Language-Team: GNOME Korea <gnome-kr-hackers@lists.kldp.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. *
#. * SECTION:application
#. * @short_description: an object representing a group of windows of the same
#. * application.
#. * @see_also: wnck_window_get_application()
#. * @stability: Unstable
#. *
#. * The #WnckApplication is a group of #WnckWindow that are all in the same
#. * application. It can be used to represent windows by applications, group
#. * windows by applications or to manipulate all windows of a particular
#. * application.
#. *
#. * A #WnckApplication is identified by the group leader of the #WnckWindow
#. * belonging to it, and new #WnckWindow are added to a #WnckApplication if and
#. * only if they have the group leader of the #WnckApplication.
#. *
#. * The #WnckApplication objects are always owned by libwnck and must not be
#. * referenced or unreferenced.
#.
#: ../libwnck/application.c:51
msgid "Untitled application"
msgstr "제목 없는 프로그램"

#: ../libwnck/pager-accessible.c:322
msgid "Workspace Switcher"
msgstr "작업 공간 바꾸기 프로그램"

#: ../libwnck/pager-accessible.c:333
msgid "Tool to switch between workspaces"
msgstr "작업 공간을 바꾸는 도구"

#: ../libwnck/pager-accessible.c:437
#, c-format
msgid "Click this to switch to workspace %s"
msgstr "작업 공간 %s(으)로 바꾸려면 누르십시오"

#: ../libwnck/pager.c:1910
#, c-format
msgid "Click to start dragging \"%s\""
msgstr "\"%s\"을(를) 끌어 옮기려면 누르십시오"

#: ../libwnck/pager.c:1913
#, c-format
msgid "Current workspace: \"%s\""
msgstr "현재 작업 공간: \"%s\""

#: ../libwnck/pager.c:1918
#, c-format
msgid "Click to switch to \"%s\""
msgstr "\"%s\"(으)로 바꾸려면 누르십시오"

#: ../libwnck/selector.c:1171
msgid "No Windows Open"
msgstr "열린 창이 없습니다"

#: ../libwnck/selector.c:1224
msgid "Window Selector"
msgstr "창 선택"

#: ../libwnck/selector.c:1225
msgid "Tool to switch between windows"
msgstr "창 사이로 옮겨 다니는 도구"

#: ../libwnck/tasklist.c:729
msgid "Window List"
msgstr "창 목록"

#: ../libwnck/tasklist.c:730
msgid "Tool to switch between visible windows"
msgstr "창 사이로 옮겨 다니는 도구"

#: ../libwnck/tasklist.c:3018
msgid "Mi_nimize All"
msgstr "모두 최소화(_N)"

#: ../libwnck/tasklist.c:3029
msgid "Un_minimize All"
msgstr "모두 최소화 취소(_M)"

#: ../libwnck/tasklist.c:3037
msgid "Ma_ximize All"
msgstr "모두 최대화(_X)"

#: ../libwnck/tasklist.c:3048
msgid "_Unmaximize All"
msgstr "모두 최대화 취소(_U)"

#: ../libwnck/tasklist.c:3060
msgid "_Close All"
msgstr "모두 닫기(_C)"

#: ../libwnck/test-pager.c:15
msgid "Use N_ROWS rows"
msgstr "<줄수>만큼의 줄 사용"

#: ../libwnck/test-pager.c:15
msgid "N_ROWS"
msgstr "<줄수>"

#: ../libwnck/test-pager.c:16
msgid "Only show current workspace"
msgstr "현재 작업 공간만 표시"

#: ../libwnck/test-pager.c:17 ../libwnck/test-tasklist.c:19
msgid "Use RTL as default direction"
msgstr "기본 방향으로 RTL 사용"

#: ../libwnck/test-pager.c:18
msgid "Show workspace names instead of workspace contents"
msgstr "작업 공간 내용이 아니라 작업 공간 이름 표시"

#: ../libwnck/test-pager.c:19
msgid "Use a vertical orientation"
msgstr "세로 방향 사용"

#. Translators: "tasklist" is the list of running applications (the window list)
#: ../libwnck/test-selector.c:12 ../libwnck/test-tasklist.c:20
msgid "Don't show window in tasklist"
msgstr "작업 목록에서 창 표시하지 않기"

#: ../libwnck/test-tasklist.c:16
msgid "Always group windows"
msgstr "항상 창 모으기"

#: ../libwnck/test-tasklist.c:17
msgid "Never group windows"
msgstr "창 모으지 않기"

#: ../libwnck/test-tasklist.c:18
msgid "Display windows from all workspaces"
msgstr "모든 작업 공간의 창 표시"

#: ../libwnck/test-tasklist.c:21
msgid "Enable Transparency"
msgstr "투명 사용"

#: ../libwnck/window-action-menu.c:417
msgid "Unmi_nimize"
msgstr "최소화 취소(_N)"

#: ../libwnck/window-action-menu.c:424
msgid "Mi_nimize"
msgstr "최소화(_N)"

#: ../libwnck/window-action-menu.c:432
msgid "Unma_ximize"
msgstr "최대화 취소(_X)"

#: ../libwnck/window-action-menu.c:439
msgid "Ma_ximize"
msgstr "최대화(_X)"

#: ../libwnck/window-action-menu.c:746 ../libwnck/workspace.c:281
#, c-format
msgid "Workspace %d"
msgstr "작업 공간 %d"

#: ../libwnck/window-action-menu.c:755 ../libwnck/window-action-menu.c:902
#, c-format
msgid "Workspace 1_0"
msgstr "작업 공간 1_0"

#: ../libwnck/window-action-menu.c:757 ../libwnck/window-action-menu.c:904
#, c-format
msgid "Workspace %s%d"
msgstr "작업 공간 %s%d"

#: ../libwnck/window-action-menu.c:1047
msgid "_Move"
msgstr "옮기기(_M)"

#: ../libwnck/window-action-menu.c:1054
msgid "_Resize"
msgstr "크기 조정(_R)"

#: ../libwnck/window-action-menu.c:1063
msgid "Always On _Top"
msgstr "항상 위(_T)"

#: ../libwnck/window-action-menu.c:1071
msgid "_Always on Visible Workspace"
msgstr "항상 보고 있는 작업 공간에 놓기(_A)"

#: ../libwnck/window-action-menu.c:1076
msgid "_Only on This Workspace"
msgstr "이 작업 공간에만 놓기(_O)"

#: ../libwnck/window-action-menu.c:1083
msgid "Move to Workspace _Left"
msgstr "작업 공간 왼쪽으로 옮기기(_L)"

#: ../libwnck/window-action-menu.c:1089
msgid "Move to Workspace R_ight"
msgstr "작업 공간 오른쪽으로 옮기기(_I)"

#: ../libwnck/window-action-menu.c:1095
msgid "Move to Workspace _Up"
msgstr "작업 공간 위로 옮기기(_U)"

#: ../libwnck/window-action-menu.c:1101
msgid "Move to Workspace _Down"
msgstr "작업 공간 아래로 옮기기(_D)"

#: ../libwnck/window-action-menu.c:1104
msgid "Move to Another _Workspace"
msgstr "다른 작업 공간으로 옮기기(_W)"

#: ../libwnck/window-action-menu.c:1124
msgid "_Close"
msgstr "닫기(_C)"

#. *
#. * SECTION:window
#. * @short_description: an object representing a window.
#. * @see_also: #WnckWorkspace, #WnckApplication, #WnckClassGroup
#. * @stability: Unstable
#. *
#. * The #WnckWindow objects are always owned by libwnck and must not be
#. * referenced or unreferenced.
#.
#: ../libwnck/window.c:50
msgid "Untitled window"
msgstr "제목없는 창"

#: ../libwnck/wnckprop.c:139
msgid "X window ID of the window to examine or modify"
msgstr "살펴보거나 바꿀 창의 X 윈도우 ID"

#: ../libwnck/wnckprop.c:139 ../libwnck/wnckprop.c:145
#: ../libwnck/wnckprop.c:154
msgid "XID"
msgstr "XID"

#. Translators: A group leader is the window that is the "owner" of a group
#. * of windows, ie: if you have multiple windows in one application, one window
#. * has some information about the application (like the application name).
#: ../libwnck/wnckprop.c:144
msgid "X window ID of the group leader of an application to examine"
msgstr "살펴볼 프로그램의 그룹 리더의 X 윈도우 ID"

#. Translators: A class is like a "family". E.g., all gvim windows are of the same class.
#: ../libwnck/wnckprop.c:148
msgid "Class resource of the class group to examine"
msgstr "살펴볼 클래스 그룹의 클래스 리소스"

#: ../libwnck/wnckprop.c:148
msgid "CLASS"
msgstr "<클래스>"

#: ../libwnck/wnckprop.c:150
msgid "NUMBER of the workspace to examine or modify"
msgstr "살펴보거나 바꿀 작업 공간의 <숫자>"

#: ../libwnck/wnckprop.c:150 ../libwnck/wnckprop.c:152
#: ../libwnck/wnckprop.c:169 ../libwnck/wnckprop.c:171
#: ../libwnck/wnckprop.c:173 ../libwnck/wnckprop.c:262
msgid "NUMBER"
msgstr "<숫자>"

#: ../libwnck/wnckprop.c:152
msgid "NUMBER of the screen to examine or modify"
msgstr "살펴보거나 바꿀 스크린의 <숫자>"

#: ../libwnck/wnckprop.c:154
msgid "Alias of --window"
msgstr "--window와 동일"

#. Translators: A class is like a "family". E.g., all gvim windows are of the same class.
#: ../libwnck/wnckprop.c:161
msgid ""
"List windows of the application/class group/workspace/screen (output format: "
"\"XID: Window Name\")"
msgstr ""
"프로그램/클래스 그룹/작업 공간/스크린의 창 목록 (출력 형식: \"XID: 창 이름\")"

#: ../libwnck/wnckprop.c:163
msgid ""
"List workspaces of the screen (output format: \"Number: Workspace Name\")"
msgstr "화면의 작업 공간 목록 (출력 형식: \"번호: 작업 공간 이름\")"

#: ../libwnck/wnckprop.c:169
msgid "Change the number of workspaces of the screen to NUMBER"
msgstr "스크린의 작업 공간 개수를 <숫자>로 바꿉니다"

#: ../libwnck/wnckprop.c:171
msgid "Change the workspace layout of the screen to use NUMBER rows"
msgstr "스크린의 작업 공간 배치를 <숫자>줄로 바꿉니다"

#: ../libwnck/wnckprop.c:173
msgid "Change the workspace layout of the screen to use NUMBER columns"
msgstr "스크린의 작업 공간 배치를 <숫자>열로 바꿉니다"

#: ../libwnck/wnckprop.c:175
msgid "Show the desktop"
msgstr "바탕 화면을 봅니다"

#: ../libwnck/wnckprop.c:177
msgid "Stop showing the desktop"
msgstr "바탕 화면을 그만 봅니다"

#. Translators: 'viewport' is kind of the viewable area. a viewport can be used to implement
#. a workspace (e.g. compiz is an example); however it is not just the current workspace.
#: ../libwnck/wnckprop.c:181
msgid "Move the viewport of the current workspace to X coordinate X"
msgstr "현재 작업 공간의 뷰포트의 가로 좌표를 <가로>로 옮깁니다"

#: ../libwnck/wnckprop.c:181 ../libwnck/wnckprop.c:264
msgid "X"
msgstr "<가로>"

#. Translators: 'viewport' is kind of the viewable area. a viewport can be used to implement
#. a workspace (e.g. compiz is an example); however it is not just the current workspace.
#: ../libwnck/wnckprop.c:185
msgid "Move the viewport of the current workspace to Y coordinate Y"
msgstr "현재 작업 공간의 뷰포트의 세로 좌표를 <세로>로 옮깁니다"

#: ../libwnck/wnckprop.c:185 ../libwnck/wnckprop.c:266
msgid "Y"
msgstr "<세로>"

#: ../libwnck/wnckprop.c:191
msgid "Minimize the window"
msgstr "창을 최소화합니다"

#: ../libwnck/wnckprop.c:193
msgid "Unminimize the window"
msgstr "창을 최소화 취소합니다"

#: ../libwnck/wnckprop.c:195
msgid "Maximize the window"
msgstr "창을 최대화합니다"

#: ../libwnck/wnckprop.c:197
msgid "Unmaximize the window"
msgstr "창을 최대화 취소합니다"

#: ../libwnck/wnckprop.c:199
msgid "Maximize horizontally the window"
msgstr "창을 가로 방향으로 최대화합니다"

#: ../libwnck/wnckprop.c:201
msgid "Unmaximize horizontally the window"
msgstr "창을 가로 방향으로 최대화 취소합니다"

#: ../libwnck/wnckprop.c:203
msgid "Maximize vertically the window"
msgstr "창을 세로 방향으로 최대화합니다"

#: ../libwnck/wnckprop.c:205
msgid "Unmaximize vertically the window"
msgstr "창을 세로 방향으로 최대화 취소합니다"

#: ../libwnck/wnckprop.c:207
msgid "Start moving the window via the keyboard"
msgstr "키보드를 이용해 창을 옮기기 시작합니다"

#: ../libwnck/wnckprop.c:209
msgid "Start resizing the window via the keyboard"
msgstr "키보드를 이용해 창의 크기를 바꾸기 시작합니다"

#: ../libwnck/wnckprop.c:211
msgid "Activate the window"
msgstr "창을 활성화합니다"

#: ../libwnck/wnckprop.c:213
msgid "Close the window"
msgstr "창을 닫습니다"

#: ../libwnck/wnckprop.c:216
msgid "Make the window fullscreen"
msgstr "창을 전체 화면으로 만듭니다"

#: ../libwnck/wnckprop.c:218
msgid "Make the window quit fullscreen mode"
msgstr "창의 전체 화면 모드를 멈춥니다"

#: ../libwnck/wnckprop.c:220
msgid "Make the window always on top"
msgstr "창을 항상 위에 놓습니다"

#: ../libwnck/wnckprop.c:222
msgid "Make the window not always on top"
msgstr "창을 항상 위에 있지 않게 합니다"

#: ../libwnck/wnckprop.c:224
msgid "Make the window below other windows"
msgstr "창을 다른 창 아래에 놓습니다"

#: ../libwnck/wnckprop.c:226
msgid "Make the window not below other windows"
msgstr "창을 다른 창 아래에 있지 않게 합니다"

#: ../libwnck/wnckprop.c:228
msgid "Shade the window"
msgstr "창을 말아올립니다"

#: ../libwnck/wnckprop.c:230
msgid "Unshade the window"
msgstr "말아올린 창을 복구합니다"

#. Translators: 'viewport' is kind of the viewable area. a viewport can be used to implement
#. a workspace (e.g. compiz is an example); however it is not just the current workspace.
#: ../libwnck/wnckprop.c:234
msgid "Make the window have a fixed position in the viewport"
msgstr "창이 뷰포트안에서 고정된 위치를 갖게 합니다"

#. Translators: 'viewport' is kind of the viewable area. a viewport can be used to implement
#. a workspace (e.g. compiz is an example); however it is not just the current workspace.
#: ../libwnck/wnckprop.c:238
msgid "Make the window not have a fixed position in the viewport"
msgstr "창이 뷰포트안에서 고정된 위치를 갖지 않게 합니다"

#. Translators: A pager is the technical term for the workspace switcher.
#. * It's a representation of all workspaces with windows inside it.
#. * Please make sure that the translation is in sync with gnome-panel,
#. * where this term is also used in translatable strings
#: ../libwnck/wnckprop.c:244
msgid "Make the window not appear in pagers"
msgstr "창이 작업 공간 표시 안에 보이지 않게 합니다"

#. Translators: A pager is the technical term for the workspace switcher.
#. * It's a representation of all workspaces with windows inside it.
#. * Please make sure that the translation is in sync with gnome-panel,
#. * where this term is also used in translatable strings
#: ../libwnck/wnckprop.c:250
msgid "Make the window appear in pagers"
msgstr "창이 작업 공간 표시 안에 보이게 합니다"

#. Translators: "tasklist" is the list of running applications (the window list)
#: ../libwnck/wnckprop.c:253
msgid "Make the window not appear in tasklists"
msgstr "창을 작업 목록에 보이지 않게 합니다"

#. Translators: "tasklist" is the list of running applications (the window list)
#: ../libwnck/wnckprop.c:256
msgid "Make the window appear in tasklists"
msgstr "창을 작업 목록에 보이게 합니다"

#: ../libwnck/wnckprop.c:258
msgid "Make the window visible on all workspaces"
msgstr "창을 모든 작업 공간에 보이게 합니다 "

#: ../libwnck/wnckprop.c:260
msgid "Make the window visible on the current workspace only"
msgstr "창을 현재 작업 공간에만 보이게 합니다 "

#: ../libwnck/wnckprop.c:262
msgid "Move the window to workspace NUMBER (first workspace is 0)"
msgstr "창을 작업공간 <숫자>로 옮깁니다 (첫번째 작업 공간이 0부터 시작)"

#: ../libwnck/wnckprop.c:264
msgid "Change the X coordinate of the window to X"
msgstr "창의 가로 좌표를 <가로>로 바꿉니다"

#: ../libwnck/wnckprop.c:266
msgid "Change the Y coordinate of the window to Y"
msgstr "창의 세로 좌표를 <세로>로 바꿉니다"

#: ../libwnck/wnckprop.c:268
msgid "Change the width of the window to WIDTH"
msgstr "창의 너비를 <너비>로 바꿉니다"

#: ../libwnck/wnckprop.c:268
msgid "WIDTH"
msgstr "<너비>"

#: ../libwnck/wnckprop.c:270
msgid "Change the height of the window to HEIGHT"
msgstr "창의 높이를 <높이>로 바꿉니다"

#: ../libwnck/wnckprop.c:270
msgid "HEIGHT"
msgstr "<높이>"

#. Translators: do not translate "normal, desktop, dock..."
#: ../libwnck/wnckprop.c:273
msgid ""
"Change the type of the window to TYPE (valid values: normal, desktop, dock, "
"dialog, toolbar, menu, utility, splash)"
msgstr ""
"창의 종류를 <종류>로 바꿉니다 (가능한 값: normal, desktop, dock, dialog, "
"toolbar, menu, utility, splash)"

#: ../libwnck/wnckprop.c:273
msgid "TYPE"
msgstr "<종류>"

#: ../libwnck/wnckprop.c:279
msgid "Change the name of the workspace to NAME"
msgstr "작업 공간의 이름을 <이름>으로 바꿉니다"

#: ../libwnck/wnckprop.c:279
msgid "NAME"
msgstr "<이름>"

#: ../libwnck/wnckprop.c:281
msgid "Activate the workspace"
msgstr "작업 공가을 활성화합니다"

#: ../libwnck/wnckprop.c:373 ../libwnck/wnckprop.c:397
#: ../libwnck/wnckprop.c:433 ../libwnck/wnckprop.c:456
#, c-format
msgid "Invalid value \"%s\" for --%s"
msgstr "--%2$s 옵션에 대해 \"%1$s\" 값이 잘못되었습니다"

#: ../libwnck/wnckprop.c:490 ../libwnck/wnckprop.c:509
#, c-format
msgid ""
"Conflicting options are present: screen %d should be interacted with, but --%"
"s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: 스크린 %d번과 통신하지만, --%s "
"옵션을 사용했습니다\n"

#: ../libwnck/wnckprop.c:499
#, c-format
msgid ""
"Conflicting options are present: windows or workspaces of screen %d should "
"be listed, but --%s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: 스크린 %d번의 창이나 작업 공간"
"의 목록을 표시해야 하지만, --%s 옵션을 사용했습니다\n"

#: ../libwnck/wnckprop.c:522 ../libwnck/wnckprop.c:542
#, c-format
msgid ""
"Conflicting options are present: workspace %d should be interacted with, but "
"--%s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: 작업 공간 %d번과 통신하지만, --"
"%s 옵션을 사용했습니다\n"

#: ../libwnck/wnckprop.c:532
#, c-format
msgid ""
"Conflicting options are present: windows of workspace %d should be listed, "
"but --%s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: 작업 공간 %d번의 창 목록을 표시"
"해야 하지만, --%s 옵션을 사용했습니다\n"

#: ../libwnck/wnckprop.c:554
#, c-format
msgid ""
"Conflicting options are present: an application should be interacted with, "
"but --%s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: 프로그램과 통신하지만, --%s 옵"
"션을 사용했습니다\n"

#: ../libwnck/wnckprop.c:564
#, c-format
msgid ""
"Conflicting options are present: windows of an application should be listed, "
"but --%s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: 프로그램의 창 목록을 표시해야 "
"하지만, --%s 옵션을 사용했습니다\n"

#. Translators: A class is like a "family". E.g., all gvim windows are of the same class.
#: ../libwnck/wnckprop.c:577
#, c-format
msgid ""
"Conflicting options are present: class group \"%s\" should be interacted "
"with, but --%s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: \"%s\" 클래스 그룹과 통신하지"
"만, --%s 옵션을 사용했습니다\n"

#. Translators: A class is like a "family". E.g., all gvim windows are of the same class.
#: ../libwnck/wnckprop.c:588
#, c-format
msgid ""
"Conflicting options are present: windows of class group \"%s\" should be "
"listed, but --%s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: \"%s\" 클래스 그룹의 창 목록을 "
"표시해야 하지만, --%s 옵션을 사용했습니다\n"

#: ../libwnck/wnckprop.c:600 ../libwnck/wnckprop.c:609
#, c-format
msgid ""
"Conflicting options are present: a window should be interacted with, but --%"
"s has been used\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: 창과 통신하지만, --%s 옵션을 사"
"용했습니다\n"

#: ../libwnck/wnckprop.c:628 ../libwnck/wnckprop.c:709
#: ../libwnck/wnckprop.c:756
#, c-format
msgid "Conflicting options are present: --%s and --%s\n"
msgstr "같이 사용할 수 없는 옵션을 같이 사용했습니다: --%s 옵션과 --%s 옵션\n"

#: ../libwnck/wnckprop.c:667
#, c-format
msgid ""
"Invalid argument \"%d\" for --%s: the argument must be strictly positive\n"
msgstr ""
"--%2$s 옵션에 대해 \"%1$d\" 인자가 잘못되었습니다: 옵션의 인자는 0보다 커야 "
"합니다\n"

#: ../libwnck/wnckprop.c:680
#, c-format
msgid "Invalid argument \"%d\" for --%s: the argument must be positive\n"
msgstr ""
"--%2$s 옵션에 대해 \"%1$d\" 인자가 잘못되었습니다: 옵션의 인자는 0보다 같거"
"나 커야 합니다\n"

#: ../libwnck/wnckprop.c:775
#, c-format
msgid "Conflicting options are present: --%s or --%s, and --%s\n"
msgstr ""
"같이 사용할 수 없는 옵션을 같이 사용했습니다: --%s 혹은 --%s 옵션과 --%s 옵"
"션\n"

#: ../libwnck/wnckprop.c:807
#, c-format
msgid "Invalid argument \"%s\" for --%s, valid values are: %s\n"
msgstr "--%2$s 옵션에 대해 \"%1$s\" 인자가 잘못되었습니다. 올바른 값은: %3$s\n"

#: ../libwnck/wnckprop.c:850
#, c-format
msgid ""
"Cannot change the workspace layout on the screen: the layout is already "
"owned\n"
msgstr ""
"스크린의 작업 공간 배치를 바꿀 수 없습니다: 다른 프로그램이 작업 공간 배치를 "
"사용 중입니다\n"

#. Translators: 'viewport' is kind of the viewable area. a viewport can be used to implement
#. a workspace (e.g. compiz is an example); however it is not just the current workspace.
#: ../libwnck/wnckprop.c:884
#, c-format
msgid ""
"Viewport cannot be moved: the current workspace does not contain a viewport\n"
msgstr "뷰포트를 옮길 수 없습니다: 현재 작업 공간에 뷰포트가 없습니다\n"

#. Translators: 'viewport' is kind of the viewable area. a viewport can be used to implement
#. a workspace (e.g. compiz is an example); however it is not just the current workspace.
#: ../libwnck/wnckprop.c:890
#, c-format
msgid "Viewport cannot be moved: there is no current workspace\n"
msgstr "뷰포트를 옮길 수 없습니다: 현재 작업 공간이 없습니다\n"

#. FIXME: why do we have dual & boolean API. This is not consistent!
#: ../libwnck/wnckprop.c:926 ../libwnck/wnckprop.c:935
#: ../libwnck/wnckprop.c:944 ../libwnck/wnckprop.c:951
#: ../libwnck/wnckprop.c:961 ../libwnck/wnckprop.c:968
#: ../libwnck/wnckprop.c:977 ../libwnck/wnckprop.c:1026
#, c-format
msgid "Action not allowed\n"
msgstr "허용하지 않는 동작입니다\n"

#: ../libwnck/wnckprop.c:1022
#, c-format
msgid "Window cannot be moved to workspace %d: the workspace does not exist\n"
msgstr "창을 작업 공간 %d번으로 옮길 수 없습니다: 그 작업 공간이 없습니다\n"

#. Translators: 'unset' in the sense of "something has not been set".
#: ../libwnck/wnckprop.c:1086 ../libwnck/wnckprop.c:1218
msgid "<name unset>"
msgstr "<이름 설정 취소>"

#. Translators: %lu is a window number and %s a window name
#: ../libwnck/wnckprop.c:1089
#, c-format
msgid "%lu: %s\n"
msgstr "%lu: %s\n"

#. Translators: %d is a workspace number and %s a workspace name
#: ../libwnck/wnckprop.c:1109
#, c-format
msgid "%d: %s\n"
msgstr "%d: %s\n"

#: ../libwnck/wnckprop.c:1172
#, c-format
msgid "Screen Number: %d\n"
msgstr "스크린 번호: %d\n"

#: ../libwnck/wnckprop.c:1174 ../libwnck/wnckprop.c:1255
#, c-format
msgid "Geometry (width, height): %d, %d\n"
msgstr "크기 (너비, 높이): %d, %d\n"

#: ../libwnck/wnckprop.c:1178
#, c-format
msgid "Number of Workspaces: %d\n"
msgstr "작업 공간 개수: %d\n"

#: ../libwnck/wnckprop.c:1184
#, c-format
msgid "Workspace Layout (rows, columns, orientation): %d, %d, %s\n"
msgstr "작업 공간 배치 (열, 행, 방향): %d, %d, %s\n"

#: ../libwnck/wnckprop.c:1194 ../libwnck/wnckprop.c:1251
#: ../libwnck/wnckprop.c:1443
msgid "<no EWMH-compliant window manager>"
msgstr "<EWMH 호환 창 관리자 없음>"

#: ../libwnck/wnckprop.c:1195
#, c-format
msgid "Window Manager: %s\n"
msgstr "창 관리자: %s\n"

#. Translators: %d is a workspace number and %s a workspace name
#: ../libwnck/wnckprop.c:1200 ../libwnck/wnckprop.c:1279
#: ../libwnck/wnckprop.c:1291 ../libwnck/wnckprop.c:1303
#: ../libwnck/wnckprop.c:1315 ../libwnck/wnckprop.c:1428
#, c-format
msgid "%d (\"%s\")"
msgstr "%d (\"%s\")"

#. Translators: "none" here means "no workspace"
#: ../libwnck/wnckprop.c:1205 ../libwnck/wnckprop.c:1284
#: ../libwnck/wnckprop.c:1296 ../libwnck/wnckprop.c:1308
#: ../libwnck/wnckprop.c:1320 ../libwnck/wnckprop.c:1435
msgctxt "workspace"
msgid "none"
msgstr "없음"

#: ../libwnck/wnckprop.c:1206
#, c-format
msgid "Active Workspace: %s\n"
msgstr "활성화한 작업 공간: %s\n"

#: ../libwnck/wnckprop.c:1215
#, c-format
msgid "\"%s\""
msgstr "\"%s\""

#. Translators: %lu is a window number and %s a window name
#: ../libwnck/wnckprop.c:1221
#, c-format
msgid "%lu (%s)"
msgstr "%lu (%s)"

#. Translators: "none" here means "no window"
#: ../libwnck/wnckprop.c:1227
msgctxt "window"
msgid "none"
msgstr "없음"

#: ../libwnck/wnckprop.c:1228
#, c-format
msgid "Active Window: %s\n"
msgstr "활성 창: %s\n"

#: ../libwnck/wnckprop.c:1231
#, c-format
msgid "Showing the desktop: %s\n"
msgstr "바탕 화면 표시: %s\n"

#: ../libwnck/wnckprop.c:1233
msgid "true"
msgstr "참"

#: ../libwnck/wnckprop.c:1233
msgid "false"
msgstr "거짓"

#: ../libwnck/wnckprop.c:1244
#, c-format
msgid "Workspace Name: %s\n"
msgstr "작업 공간 이름: %s\n"

#: ../libwnck/wnckprop.c:1245
#, c-format
msgid "Workspace Number: %d\n"
msgstr "작업 공간 번호: %d\n"

#: ../libwnck/wnckprop.c:1252 ../libwnck/wnckprop.c:1444
#, c-format
msgid "On Screen: %d (Window Manager: %s)\n"
msgstr "스크린: %d (창 관리자: %s)\n"

#. Translators: 'viewport' is kind of the viewable area. a viewport can be used to implement
#. a workspace (e.g. compiz is an example); however it is not just the current workspace.
#: ../libwnck/wnckprop.c:1266
msgid "<no viewport>"
msgstr "<뷰포트 없음>"

#. Translators: 'viewport' is kind of the viewable area. a viewport can be used to implement
#. a workspace (e.g. compiz is an example); however it is not just the current workspace.
#: ../libwnck/wnckprop.c:1269
#, c-format
msgid "Viewport position (x, y): %s\n"
msgstr "뷰포트 위치 (가로, 세로): %s\n"

#: ../libwnck/wnckprop.c:1272
#, c-format
msgid "Position in Layout (row, column): %d, %d\n"
msgstr "배치 상의 위치 (줄, 열): %d, %d\n"

#: ../libwnck/wnckprop.c:1285
#, c-format
msgid "Left Neighbor: %s\n"
msgstr "왼쪽 이웃: %s\n"

#: ../libwnck/wnckprop.c:1297
#, c-format
msgid "Right Neighbor: %s\n"
msgstr "오른쪽 이웃: %s\n"

#: ../libwnck/wnckprop.c:1309
#, c-format
msgid "Top Neighbor: %s\n"
msgstr "위 이웃: %s\n"

#: ../libwnck/wnckprop.c:1321
#, c-format
msgid "Bottom Neighbor: %s\n"
msgstr "아래 이웃: %s\n"

#. Translators: Ressource class is the name to identify a class.
#: ../libwnck/wnckprop.c:1333
#, c-format
msgid "Resource Class: %s\n"
msgstr "리소스 클래스: %s\n"

#: ../libwnck/wnckprop.c:1335
#, c-format
msgid "Group Name: %s\n"
msgstr "그룹 이름: %s\n"

#. Translators: 'set' in the sense of "something has been set".
#: ../libwnck/wnckprop.c:1341 ../libwnck/wnckprop.c:1365
#: ../libwnck/wnckprop.c:1419
msgid "set"
msgstr "설정"

#. Translators: 'unset' in the sense of "something has not been set".
#: ../libwnck/wnckprop.c:1344 ../libwnck/wnckprop.c:1368
#: ../libwnck/wnckprop.c:1375 ../libwnck/wnckprop.c:1405
#: ../libwnck/wnckprop.c:1412 ../libwnck/wnckprop.c:1422
#: ../libwnck/wnckprop.c:1487 ../libwnck/wnckprop.c:1497
#: ../libwnck/wnckprop.c:1505
msgid "<unset>"
msgstr "<설정취소>"

#: ../libwnck/wnckprop.c:1345 ../libwnck/wnckprop.c:1369
#: ../libwnck/wnckprop.c:1423
#, c-format
msgid "Icons: %s\n"
msgstr "아이콘: %s\n"

#: ../libwnck/wnckprop.c:1348 ../libwnck/wnckprop.c:1386
#, c-format
msgid "Number of Windows: %d\n"
msgstr "창 개수: %d\n"

#: ../libwnck/wnckprop.c:1360 ../libwnck/wnckprop.c:1406
#, c-format
msgid "Name: %s\n"
msgstr "이름: %s\n"

#. Translators: note that "Icon" here has a specific window
#. * management-related meaning. It means minimized.
#: ../libwnck/wnckprop.c:1361 ../libwnck/wnckprop.c:1415
#, c-format
msgid "Icon Name: %s\n"
msgstr "아이콘 이름: %s\n"

#: ../libwnck/wnckprop.c:1376 ../libwnck/wnckprop.c:1498
#, c-format
msgid "PID: %s\n"
msgstr "PID: %s\n"

#. Translators: "none" here means "no startup ID"
#: ../libwnck/wnckprop.c:1383
msgctxt "startupID"
msgid "none"
msgstr "없음"

#: ../libwnck/wnckprop.c:1384
#, c-format
msgid "Startup ID: %s\n"
msgstr "시작 ID: %s\n"

#: ../libwnck/wnckprop.c:1432
msgid "all workspaces"
msgstr "모든 작업 공간"

#: ../libwnck/wnckprop.c:1436
#, c-format
msgid "On Workspace: %s\n"
msgstr "작업 공간: %s\n"

#: ../libwnck/wnckprop.c:1451
msgid "normal window"
msgstr "일반 창"

#: ../libwnck/wnckprop.c:1454
msgid "desktop"
msgstr "바탕 화면"

#: ../libwnck/wnckprop.c:1457
msgid "dock or panel"
msgstr "도크 혹은 패널"

#: ../libwnck/wnckprop.c:1460
msgid "dialog window"
msgstr "대화 창"

#: ../libwnck/wnckprop.c:1463
msgid "tearoff toolbar"
msgstr "떼어낸 툴바"

#: ../libwnck/wnckprop.c:1466
msgid "tearoff menu"
msgstr "떼어낸 메뉴"

#: ../libwnck/wnckprop.c:1469
msgid "utility window"
msgstr "유틸리티 창"

#: ../libwnck/wnckprop.c:1472
msgid "splash screen"
msgstr "스플래시 창"

#: ../libwnck/wnckprop.c:1477
#, c-format
msgid "Window Type: %s\n"
msgstr "창 종류: %s\n"

#: ../libwnck/wnckprop.c:1480
#, c-format
msgid "Geometry (x, y, width, height): %d, %d, %d, %d\n"
msgstr "위치 및 크기 (가로, 세로, 너비, 높이): %d, %d, %d, %d\n"

#. Translators: A class is like a "family". E.g., all gvim windows are of the same class.
#: ../libwnck/wnckprop.c:1489
#, c-format
msgid "Class Group: %s\n"
msgstr "클래스 그룹: %s\n"

#: ../libwnck/wnckprop.c:1491
#, c-format
msgid "XID: %lu\n"
msgstr "XID: %lu\n"

#: ../libwnck/wnckprop.c:1506
#, c-format
msgid "Session ID: %s\n"
msgstr "세션 ID: %s\n"

#. Translators: A group leader is the window that is the "owner" of a group
#. * of windows, ie: if you have multiple windows in one application, one window
#. * has some information about the application (like the application name).
#: ../libwnck/wnckprop.c:1512
#, c-format
msgid "Group Leader: %lu\n"
msgstr "그룹 리더: %lu\n"

#. Translators: A window can be transient for another window: it means it's on top of it
#: ../libwnck/wnckprop.c:1517
#, c-format
msgid "Transient for: %lu\n"
msgstr "아래 놓임: %lu\n"

#. FIXME: else print something?
#. Translators: we're building a list of items here.             * For example, the result is "a, b".                            * In this case, the first string is "a", the second             * string is ", " and the third string is "b".                   * We can then use this information here to also                 * recursively build longer lists, like "a, b, c, d"
#. Translators: we're building a list of items here.             * The end result is something like "a, b, c"                    * In this case, the first string is "a, b", the second          * string is ", " and the third string is "c"
#: ../libwnck/wnckprop.c:1532 ../libwnck/wnckprop.c:1579
#, c-format
msgid "%1$s%2$s%3$s"
msgstr "%1$s%2$s%3$s"

#. Translators: see comment for "%1$s%2$s%3$s" in order          * to properly translate this
#: ../libwnck/wnckprop.c:1536 ../libwnck/wnckprop.c:1581
msgid ", "
msgstr ", "

#: ../libwnck/wnckprop.c:1542
msgid "minimized"
msgstr "최소화"

#: ../libwnck/wnckprop.c:1543
msgid "maximized"
msgstr "최대화"

#: ../libwnck/wnckprop.c:1547
msgid "maximized horizontally"
msgstr "가로 최대화"

#: ../libwnck/wnckprop.c:1549
msgid "maximized vertically"
msgstr "세로 최대화"

#: ../libwnck/wnckprop.c:1551
msgid "shaded"
msgstr "말아올림"

#: ../libwnck/wnckprop.c:1552
msgid "pinned"
msgstr "위치 고정"

#: ../libwnck/wnckprop.c:1553
msgid "sticky"
msgstr "고정"

#: ../libwnck/wnckprop.c:1554
msgid "above"
msgstr "위"

#: ../libwnck/wnckprop.c:1555
msgid "below"
msgstr "아래"

#: ../libwnck/wnckprop.c:1556
msgid "fullscreen"
msgstr "전체 화면"

#: ../libwnck/wnckprop.c:1557
msgid "needs attention"
msgstr "주의 필요"

#. Translators: A pager is the technical term for the workspace switcher.
#. * It's a representation of all workspaces with windows inside it.
#. * Please make sure that the translation is in sync with gnome-panel,
#. * where this term is also used in translatable strings
#: ../libwnck/wnckprop.c:1562
msgid "skip pager"
msgstr "작업 공간 표시 건너뛰기"

#. Translators: "tasklist" is the list of running applications (the window list)
#: ../libwnck/wnckprop.c:1564
msgid "skip tasklist"
msgstr "작업 목록 건너뛰기"

#: ../libwnck/wnckprop.c:1566
msgid "normal"
msgstr "일반"

#: ../libwnck/wnckprop.c:1567
#, c-format
msgid "State: %s\n"
msgstr "상태: %s\n"

#: ../libwnck/wnckprop.c:1588
msgid "move"
msgstr "옮기기"

#: ../libwnck/wnckprop.c:1589
msgid "resize"
msgstr "크기 조정"

#: ../libwnck/wnckprop.c:1590
msgid "shade"
msgstr "말아올리기"

#: ../libwnck/wnckprop.c:1591
msgid "unshade"
msgstr "끌어내리기"

#: ../libwnck/wnckprop.c:1592
msgid "stick"
msgstr "고정"

#: ../libwnck/wnckprop.c:1593
msgid "unstick"
msgstr "고정 취소"

#: ../libwnck/wnckprop.c:1595
msgid "maximize horizontally"
msgstr "가로 최대화"

#: ../libwnck/wnckprop.c:1597
msgid "unmaximize horizontally"
msgstr "가로 최대화 취소"

#: ../libwnck/wnckprop.c:1599
msgid "maximize vertically"
msgstr "세로 최대화"

#: ../libwnck/wnckprop.c:1601
msgid "unmaximize vertically"
msgstr "세로 최대화 취소"

#: ../libwnck/wnckprop.c:1604
msgid "change workspace"
msgstr "작업 공간 바꾸기"

#: ../libwnck/wnckprop.c:1606
msgid "pin"
msgstr "위치 고정"

#: ../libwnck/wnckprop.c:1608
msgid "unpin"
msgstr "위치 고정 해제"

#: ../libwnck/wnckprop.c:1609
msgid "minimize"
msgstr "최소화"

#: ../libwnck/wnckprop.c:1610
msgid "unminimize"
msgstr "최소화 취소"

#: ../libwnck/wnckprop.c:1611
msgid "maximize"
msgstr "최대화"

#: ../libwnck/wnckprop.c:1612
msgid "unmaximize"
msgstr "최대화 취소"

#: ../libwnck/wnckprop.c:1614
msgid "change fullscreen mode"
msgstr "전체 화면 모드 바꾸기"

#: ../libwnck/wnckprop.c:1615
msgid "close"
msgstr "닫기"

#: ../libwnck/wnckprop.c:1617
msgid "make above"
msgstr "위로 놓기"

#: ../libwnck/wnckprop.c:1619
msgid "unmake above"
msgstr "위로 놓기 취소"

#: ../libwnck/wnckprop.c:1621
msgid "make below"
msgstr "아래로 놓기"

#: ../libwnck/wnckprop.c:1623
msgid "unmake below"
msgstr "아래로 놓기 취소"

#: ../libwnck/wnckprop.c:1625
msgid "no action possible"
msgstr "가능한 동작 없음"

#: ../libwnck/wnckprop.c:1626
#, c-format
msgid "Possible Actions: %s\n"
msgstr "가능한 동작: %s\n"

#: ../libwnck/wnckprop.c:1805
msgid ""
"Print or modify the properties of a screen/workspace/window, or interact "
"with it, following the EWMH specification.\n"
"For information about this specification, see:\n"
"\thttp://freedesktop.org/wiki/Specifications/wm-spec"
msgstr ""
"스크린/작업 공간/창의 속성을 표시하거나 바꿉니다. 혹은 EWMH 명세에 따라 스크"
"린/작업 공간/창과 통신합니다.\n"
"EWMH 명세에 관한 정보는 다음을 참고하십시오:\n"
"\thttp://freedesktop.org/wiki/Specifications/wm-spec"

#: ../libwnck/wnckprop.c:1815
msgid "Options to list windows or workspaces"
msgstr "창이나 작업 공간 목록이 들어 있는 옵션"

#: ../libwnck/wnckprop.c:1816
msgid "Show options to list windows or workspaces"
msgstr "창이나 작업 공간 목록이 들어 있는 옵션을 표시합니다"

#: ../libwnck/wnckprop.c:1823
msgid "Options to modify properties of a window"
msgstr "창의 속성을 수정하는 옵션"

#: ../libwnck/wnckprop.c:1824
msgid "Show options to modify properties of a window"
msgstr "창의 속성을 수정하는 옵션 표시"

#: ../libwnck/wnckprop.c:1831
msgid "Options to modify properties of a workspace"
msgstr "작업 공간의 속성을 수정하는 옵션"

#: ../libwnck/wnckprop.c:1832
msgid "Show options to modify properties of a workspace"
msgstr "작업 공간의 속성을 수정하는 옵션 표시"

#: ../libwnck/wnckprop.c:1839
msgid "Options to modify properties of a screen"
msgstr "화면의 속성을 수정하는 옵션"

#: ../libwnck/wnckprop.c:1840
msgid "Show options to modify properties of a screen"
msgstr "화면의 속성을 수정하는 옵션 표시"

#: ../libwnck/wnckprop.c:1851
#, c-format
msgid "Error while parsing arguments: %s\n"
msgstr "인자를 파싱하는 데 오류가 발생했습니다: %s\n"

#: ../libwnck/wnckprop.c:1874
#, c-format
msgid "Cannot interact with screen %d: the screen does not exist\n"
msgstr "스크린 %d번과 통신할 수 없습니다: 그 스크린이 없습니다\n"

#: ../libwnck/wnckprop.c:1930
#, c-format
msgid "Cannot interact with workspace %d: the workspace cannot be found\n"
msgstr "작업 공간 %d번과 통신할 수 없습니다: 그 작업 공간이 없습니다\n"

#. Translators: A class is like a "family". E.g., all gvim windows are of the same class.
#: ../libwnck/wnckprop.c:1953
#, c-format
msgid ""
"Cannot interact with class group \"%s\": the class group cannot be found\n"
msgstr "\"%s\" 클래스 그룹과 통신할 수 없습니다: 그 클래스 그룹이 없습니다\n"

#: ../libwnck/wnckprop.c:1976
#, c-format
msgid ""
"Cannot interact with application having its group leader with XID %lu: the "
"application cannot be found\n"
msgstr ""
"그룹 리더의 XID가 %lu인 프로그램과 통신할 수 없습니다: 그 프로그램이 없습니"
"다\n"

#: ../libwnck/wnckprop.c:1999
#, c-format
msgid "Cannot interact with window with XID %lu: the window cannot be found\n"
msgstr "XID가 %lu인 창과 통신할 수 없습니다: 그 창이 없습니다\n"

#~ msgid "workspace|none"
#~ msgstr "없음"

#~ msgid "window|none"
#~ msgstr "없음"

#~ msgid "startupID|none"
#~ msgstr "없음"
