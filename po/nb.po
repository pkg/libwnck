# Norwegian translation of libwnck
# Copyright (C) 2001-2003 Free Software Foundation, Inc.
# Kjartan Maraas <kmaraas@gnome.org>, 2001-2010.
#
msgid ""
msgstr ""
"Project-Id-Version: libwnck 2.31.x\n"
"Report-Msgid-Bugs-To:\n"
"POT-Creation-Date: 2010-06-20 12:11+0200\n"
"PO-Revision-Date: 2010-06-20 12:13+0200\n"
"Last-Translator: Kjartan Maraas <kmaraas@gnome.org>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.n>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. *
#. * SECTION:application
#. * @short_description: an object representing a group of windows of the same
#. * application.
#. * @see_also: wnck_window_get_application()
#. * @stability: Unstable
#. *
#. * The #WnckApplication is a group of #WnckWindow that are all in the same
#. * application. It can be used to represent windows by applications, group
#. * windows by applications or to manipulate all windows of a particular
#. * application.
#. *
#. * A #WnckApplication is identified by the group leader of the #WnckWindow
#. * belonging to it, and new #WnckWindow are added to a #WnckApplication if and
#. * only if they have the group leader of the #WnckApplication.
#. *
#. * The #WnckApplication objects are always owned by libwnck and must not be
#. * referenced or unreferenced.
#.
#: ../libwnck/application.c:51
msgid "Untitled application"
msgstr "Program uten navn"

#: ../libwnck/pager-accessible.c:338
msgid "Workspace Switcher"
msgstr "Arbeidsområdebytter"

#: ../libwnck/pager-accessible.c:349
msgid "Tool to switch between workspaces"
msgstr "Verktøy for å bytte mellom arbeidsområder"

#: ../libwnck/pager-accessible.c:461
#, c-format
msgid "Click this to switch to workspace %s"
msgstr "Klikk her for å bytte til arbeidsområde %s"

#: ../libwnck/pager.c:1965
#, c-format
msgid "Click to start dragging \"%s\""
msgstr "Klikk for å dra «%s»"

#: ../libwnck/pager.c:1968
#, c-format
msgid "Current workspace: \"%s\""
msgstr "Aktivt arbeidsområde: «%s»"

#: ../libwnck/pager.c:1973
#, c-format
msgid "Click to switch to \"%s\""
msgstr "Klikk for å bytte til «%s»"

#: ../libwnck/selector.c:1177
msgid "No Windows Open"
msgstr "Ingen åpne vinduer"

#: ../libwnck/selector.c:1230
msgid "Window Selector"
msgstr "Vinduvelger"

#: ../libwnck/selector.c:1231
msgid "Tool to switch between windows"
msgstr "Verktøy for å bytte mellom vinduer"

#: ../libwnck/tasklist.c:733
msgid "Window List"
msgstr "Vinduliste"

#: ../libwnck/tasklist.c:734
msgid "Tool to switch between visible windows"
msgstr "Verktøy for å bytte mellom synlige vinduer"

#: ../libwnck/tasklist.c:3062
msgid "Mi_nimize All"
msgstr "Mi_nimer alle"

#: ../libwnck/tasklist.c:3073
msgid "Un_minimize All"
msgstr "Gjen_opprett alle"

#: ../libwnck/tasklist.c:3081
msgid "Ma_ximize All"
msgstr "Ma_ksimer alle"

#: ../libwnck/tasklist.c:3092
msgid "_Unmaximize All"
msgstr "Gjeno_pprett alle"

#: ../libwnck/tasklist.c:3104
msgid "_Close All"
msgstr "_Lukk alle"

#: ../libwnck/test-pager.c:15
msgid "Use N_ROWS rows"
msgstr "Bruk N_ROWS rader"

#: ../libwnck/test-pager.c:15
msgid "N_ROWS"
msgstr "N_ROWS"

#: ../libwnck/test-pager.c:16
msgid "Only show current workspace"
msgstr "Kun vis aktivt arbeidsområde"

#: ../libwnck/test-pager.c:17 ../libwnck/test-tasklist.c:19
msgid "Use RTL as default direction"
msgstr "Bruk RTL som forvalgt retning"

#: ../libwnck/test-pager.c:18
msgid "Show workspace names instead of workspace contents"
msgstr "Vis navn på arbeidsområde i stedet for innholdet"

#: ../libwnck/test-pager.c:19
msgid "Use a vertical orientation"
msgstr "Bruk vertikal orientering"

#. Translators: "tasklist" is the list of running applications (the
#. * window list)
#: ../libwnck/test-selector.c:13 ../libwnck/test-tasklist.c:20
msgid "Don't show window in tasklist"
msgstr "Ikke vis vindu i oppgavelisten"

#: ../libwnck/test-tasklist.c:16
msgid "Always group windows"
msgstr "Alltid grupper vinduer"

#: ../libwnck/test-tasklist.c:17
msgid "Never group windows"
msgstr "Aldri grupper vinduer"

#: ../libwnck/test-tasklist.c:18
msgid "Display windows from all workspaces"
msgstr "Vis vinduer fra alle arbeidsområder"

#: ../libwnck/test-tasklist.c:21
msgid "Enable Transparency"
msgstr "Aktiver gjennomsiktighet"

#: ../libwnck/window-action-menu.c:419
msgid "Unmi_nimize"
msgstr "Gje_nopprett"

#: ../libwnck/window-action-menu.c:426
msgid "Mi_nimize"
msgstr "Mi_nimer"

#: ../libwnck/window-action-menu.c:434
msgid "Unma_ximize"
msgstr "_Gjenopprett"

#: ../libwnck/window-action-menu.c:441
msgid "Ma_ximize"
msgstr "Ma_ksimer"

#: ../libwnck/window-action-menu.c:748 ../libwnck/workspace.c:281
#, c-format
msgid "Workspace %d"
msgstr "Arbeidsområde %d"

#: ../libwnck/window-action-menu.c:757 ../libwnck/window-action-menu.c:904
#, c-format
msgid "Workspace 1_0"
msgstr "Arbeidsområde 1_0"

#: ../libwnck/window-action-menu.c:759 ../libwnck/window-action-menu.c:906
#, c-format
msgid "Workspace %s%d"
msgstr "Arbeidsområde %s%d"

#: ../libwnck/window-action-menu.c:1049
msgid "_Move"
msgstr "F_lytt"

#: ../libwnck/window-action-menu.c:1056
msgid "_Resize"
msgstr "End_re størrelse"

#: ../libwnck/window-action-menu.c:1065
msgid "Always On _Top"
msgstr "Alltid øvers_t"

#: ../libwnck/window-action-menu.c:1073
msgid "_Always on Visible Workspace"
msgstr "_Alltid på synlig arbeidsområde"

#: ../libwnck/window-action-menu.c:1078
msgid "_Only on This Workspace"
msgstr "K_un på dette arbeidsområdet"

#: ../libwnck/window-action-menu.c:1085
msgid "Move to Workspace _Left"
msgstr "Flytt til arbeidsområdet til _venstre"

#: ../libwnck/window-action-menu.c:1091
msgid "Move to Workspace R_ight"
msgstr "Flytt til arbeidsområdet til hø_yre"

#: ../libwnck/window-action-menu.c:1097
msgid "Move to Workspace _Up"
msgstr "Flytt til et arbeidsområdet _over"

#: ../libwnck/window-action-menu.c:1103
msgid "Move to Workspace _Down"
msgstr "Flytt til arbeidsområdet un_der"

#: ../libwnck/window-action-menu.c:1106
msgid "Move to Another _Workspace"
msgstr "Flytt til et annet _arbeidsområde"

#: ../libwnck/window-action-menu.c:1126
msgid "_Close"
msgstr "_Lukk"

#. *
#. * SECTION:window
#. * @short_description: an object representing a window.
#. * @see_also: #WnckWorkspace, #WnckApplication, #WnckClassGroup
#. * @stability: Unstable
#. *
#. * The #WnckWindow objects are always owned by libwnck and must not be
#. * referenced or unreferenced.
#.
#: ../libwnck/window.c:50
msgid "Untitled window"
msgstr "Vindu uten tittel"

#: ../libwnck/wnckprop.c:139
msgid "X window ID of the window to examine or modify"
msgstr "X-vindu ID for vinduet som skal undersøkes eller endres"

#: ../libwnck/wnckprop.c:139 ../libwnck/wnckprop.c:146
#: ../libwnck/wnckprop.c:156
msgid "XID"
msgstr "XID"

#. Translators: A group leader is the window that is the "owner" of a
#. * group of windows, ie: if you have multiple windows in one
#. * application, one window has some information about the application
#. * (like the application name).
#: ../libwnck/wnckprop.c:145
msgid "X window ID of the group leader of an application to examine"
msgstr "X-vindu ID for et programs gruppeleder som skal undersøkes"

#. Translators: A class is like a "family". E.g., all gvim windows
#. * are of the same class.
#: ../libwnck/wnckprop.c:150
msgid "Class resource of the class group to examine"
msgstr "Klasseressurs for klassegruppen som skal undersøkes"

#: ../libwnck/wnckprop.c:150
msgid "CLASS"
msgstr "KLASSE"

#: ../libwnck/wnckprop.c:152
msgid "NUMBER of the workspace to examine or modify"
msgstr "NUMMER på arbeidsområdet som skal undersøkes eller endres"

#: ../libwnck/wnckprop.c:152 ../libwnck/wnckprop.c:154
#: ../libwnck/wnckprop.c:172 ../libwnck/wnckprop.c:174
#: ../libwnck/wnckprop.c:176 ../libwnck/wnckprop.c:273
msgid "NUMBER"
msgstr "ANTALL"

#: ../libwnck/wnckprop.c:154
msgid "NUMBER of the screen to examine or modify"
msgstr "NUMMER på skjermen som skal undersøkes eller endres"

#: ../libwnck/wnckprop.c:156
msgid "Alias of --window"
msgstr "Alias for --window"

#. Translators: A class is like a "family". E.g., all gvim windows
#. * are of the same class.
#: ../libwnck/wnckprop.c:164
msgid ""
"List windows of the application/class group/workspace/screen (output format: "
"\"XID: Window Name\")"
msgstr ""

#: ../libwnck/wnckprop.c:166
msgid ""
"List workspaces of the screen (output format: \"Number: Workspace Name\")"
msgstr ""

#: ../libwnck/wnckprop.c:172
msgid "Change the number of workspaces of the screen to NUMBER"
msgstr "Endre antall arbeidsområder på skjermen til TALL"

#: ../libwnck/wnckprop.c:174
msgid "Change the workspace layout of the screen to use NUMBER rows"
msgstr ""

#: ../libwnck/wnckprop.c:176
msgid "Change the workspace layout of the screen to use NUMBER columns"
msgstr ""

#: ../libwnck/wnckprop.c:178
msgid "Show the desktop"
msgstr "Vis skrivebordet"

#: ../libwnck/wnckprop.c:180
msgid "Stop showing the desktop"
msgstr "Ikke vis skrivebordet"

#. Translators: 'viewport' is kind of the viewable area. A viewport
#. * can be used to implement workspaces (e.g. compiz is an example);
#. * however it is not just the current workspace.
#: ../libwnck/wnckprop.c:185
msgid "Move the viewport of the current workspace to X coordinate X"
msgstr ""

#: ../libwnck/wnckprop.c:185 ../libwnck/wnckprop.c:275
msgid "X"
msgstr "X"

#. Translators: 'viewport' is kind of the viewable area. A viewport
#. * can be used to implement workspaces (e.g. compiz is an example);
#. * however it is not just the current workspace.
#: ../libwnck/wnckprop.c:190
msgid "Move the viewport of the current workspace to Y coordinate Y"
msgstr ""

#: ../libwnck/wnckprop.c:190 ../libwnck/wnckprop.c:277
msgid "Y"
msgstr "Y"

#: ../libwnck/wnckprop.c:196
msgid "Minimize the window"
msgstr "Minimer vinduet"

#: ../libwnck/wnckprop.c:198
msgid "Unminimize the window"
msgstr "Gjenopprett vinduet"

#: ../libwnck/wnckprop.c:200
msgid "Maximize the window"
msgstr "Maksimer vinduet"

#: ../libwnck/wnckprop.c:202
msgid "Unmaximize the window"
msgstr "Gjenopprett vinduet"

#: ../libwnck/wnckprop.c:204
msgid "Maximize horizontally the window"
msgstr "Maksimer vinduet horisontalt"

#: ../libwnck/wnckprop.c:206
msgid "Unmaximize horizontally the window"
msgstr "Gjenopprett vinduet horisontalt"

#: ../libwnck/wnckprop.c:208
msgid "Maximize vertically the window"
msgstr "Maksimer vinduet vertikalt"

#: ../libwnck/wnckprop.c:210
msgid "Unmaximize vertically the window"
msgstr "Gjenopprett vinduet vertikalt"

#: ../libwnck/wnckprop.c:212
msgid "Start moving the window via the keyboard"
msgstr "Flytt vinduet med tastaturet"

#: ../libwnck/wnckprop.c:214
msgid "Start resizing the window via the keyboard"
msgstr "Endre størrelse på vinduet med tastaturet"

#: ../libwnck/wnckprop.c:216
msgid "Activate the window"
msgstr "Aktiver vinduet"

#: ../libwnck/wnckprop.c:218
msgid "Close the window"
msgstr "Lukk vinduet"

#: ../libwnck/wnckprop.c:221
msgid "Make the window fullscreen"
msgstr "Gjør vinduet fullskjerm"

#: ../libwnck/wnckprop.c:223
msgid "Make the window quit fullscreen mode"
msgstr ""

#: ../libwnck/wnckprop.c:225
msgid "Make the window always on top"
msgstr ""

#: ../libwnck/wnckprop.c:227
msgid "Make the window not always on top"
msgstr ""

#: ../libwnck/wnckprop.c:229
msgid "Make the window below other windows"
msgstr "Plasser vinduet under andre vinduer"

#: ../libwnck/wnckprop.c:231
msgid "Make the window not below other windows"
msgstr "Ikke plasser vinduet under andre vinduer"

#: ../libwnck/wnckprop.c:233
msgid "Shade the window"
msgstr "Rull opp vinduet"

#: ../libwnck/wnckprop.c:235
msgid "Unshade the window"
msgstr "Rull ned vinduet"

#. Translators: 'viewport' is kind of the viewable area. A viewport
#. * can be used to implement workspaces (e.g. compiz is an example);
#. * however it is not just the current workspace.
#: ../libwnck/wnckprop.c:240
msgid "Make the window have a fixed position in the viewport"
msgstr "Plassert vinduet med en fast plass i visningsområdet"

#. Translators: 'viewport' is kind of the viewable area. A viewport
#. * can be used to implement workspaces (e.g. compiz is an example);
#. * however it is not just the current workspace.
#: ../libwnck/wnckprop.c:245
msgid "Make the window not have a fixed position in the viewport"
msgstr ""

#. Translators: A pager is the technical term for the workspace
#. * switcher. It's a representation of all workspaces with windows
#. * inside it. Please make sure that the translation is in sync with
#. * gnome-panel, where this term is also used in translatable strings
#.
#: ../libwnck/wnckprop.c:252
msgid "Make the window not appear in pagers"
msgstr ""

#. Translators: A pager is the technical term for the workspace
#. * switcher. It's a representation of all workspaces with windows
#. * inside it. Please make sure that the translation is in sync with
#. * gnome-panel, where this term is also used in translatable strings
#.
#: ../libwnck/wnckprop.c:259
msgid "Make the window appear in pagers"
msgstr ""

#. Translators: "tasklist" is the list of running applications (the
#. * window list)
#: ../libwnck/wnckprop.c:263
msgid "Make the window not appear in tasklists"
msgstr "Ikke gjør vinduet synlig i oppgavelisten"

#. Translators: "tasklist" is the list of running applications (the
#. * window list)
#: ../libwnck/wnckprop.c:267
msgid "Make the window appear in tasklists"
msgstr "Gjør vinduet synlig i oppgavelisten"

#: ../libwnck/wnckprop.c:269
msgid "Make the window visible on all workspaces"
msgstr "Gjør vinduet synlig på alle arbeidsområder"

#: ../libwnck/wnckprop.c:271
msgid "Make the window visible on the current workspace only"
msgstr ""

#: ../libwnck/wnckprop.c:273
msgid "Move the window to workspace NUMBER (first workspace is 0)"
msgstr ""

#: ../libwnck/wnckprop.c:275
msgid "Change the X coordinate of the window to X"
msgstr "Endre X-koordinaten for vinduet til X"

#: ../libwnck/wnckprop.c:277
msgid "Change the Y coordinate of the window to Y"
msgstr "Endre Y-koordinaten for vinduet til Y"

#: ../libwnck/wnckprop.c:279
msgid "Change the width of the window to WIDTH"
msgstr "Endre bredden på vinduet til BREDDE"

#: ../libwnck/wnckprop.c:279
msgid "WIDTH"
msgstr "BREDDE"

#: ../libwnck/wnckprop.c:281
msgid "Change the height of the window to HEIGHT"
msgstr "Endre høyden på vinduet til HØYDE"

#: ../libwnck/wnckprop.c:281
msgid "HEIGHT"
msgstr "HØYDE"

#. Translators: do not translate "normal, desktop, dock..."
#: ../libwnck/wnckprop.c:284
msgid ""
"Change the type of the window to TYPE (valid values: normal, desktop, dock, "
"dialog, toolbar, menu, utility, splash)"
msgstr ""

#: ../libwnck/wnckprop.c:284
msgid "TYPE"
msgstr "TYPE"

#: ../libwnck/wnckprop.c:290
msgid "Change the name of the workspace to NAME"
msgstr "Endre navn på arbeidsområdet til NAVN"

#: ../libwnck/wnckprop.c:290
msgid "NAME"
msgstr "NAVN"

#: ../libwnck/wnckprop.c:292
msgid "Activate the workspace"
msgstr "Aktiver arbeidsområdet"

#: ../libwnck/wnckprop.c:384 ../libwnck/wnckprop.c:408
#: ../libwnck/wnckprop.c:444 ../libwnck/wnckprop.c:467
#, c-format
msgid "Invalid value \"%s\" for --%s"
msgstr "Ugyldig verdi «%s» for --%s"

#: ../libwnck/wnckprop.c:501 ../libwnck/wnckprop.c:520
#, c-format
msgid ""
"Conflicting options are present: screen %d should be interacted with, but --%"
"s has been used\n"
msgstr ""

#: ../libwnck/wnckprop.c:510
#, c-format
msgid ""
"Conflicting options are present: windows or workspaces of screen %d should "
"be listed, but --%s has been used\n"
msgstr ""

#: ../libwnck/wnckprop.c:533 ../libwnck/wnckprop.c:553
#, c-format
msgid ""
"Conflicting options are present: workspace %d should be interacted with, but "
"--%s has been used\n"
msgstr ""

#: ../libwnck/wnckprop.c:543
#, c-format
msgid ""
"Conflicting options are present: windows of workspace %d should be listed, "
"but --%s has been used\n"
msgstr ""

#: ../libwnck/wnckprop.c:565
#, c-format
msgid ""
"Conflicting options are present: an application should be interacted with, "
"but --%s has been used\n"
msgstr ""

#: ../libwnck/wnckprop.c:575
#, c-format
msgid ""
"Conflicting options are present: windows of an application should be listed, "
"but --%s has been used\n"
msgstr ""

#. Translators: A class is like a "family". E.g., all gvim windows
#. * are of the same class.
#: ../libwnck/wnckprop.c:589
#, c-format
msgid ""
"Conflicting options are present: class group \"%s\" should be interacted "
"with, but --%s has been used\n"
msgstr ""

#. Translators: A class is like a "family". E.g., all gvim windows
#. * are of the same class.
#: ../libwnck/wnckprop.c:601
#, c-format
msgid ""
"Conflicting options are present: windows of class group \"%s\" should be "
"listed, but --%s has been used\n"
msgstr ""

#: ../libwnck/wnckprop.c:613 ../libwnck/wnckprop.c:622
#, c-format
msgid ""
"Conflicting options are present: a window should be interacted with, but --%"
"s has been used\n"
msgstr ""

#: ../libwnck/wnckprop.c:641 ../libwnck/wnckprop.c:722
#: ../libwnck/wnckprop.c:769
#, c-format
msgid "Conflicting options are present: --%s and --%s\n"
msgstr ""

#: ../libwnck/wnckprop.c:680
#, c-format
msgid ""
"Invalid argument \"%d\" for --%s: the argument must be strictly positive\n"
msgstr ""

#: ../libwnck/wnckprop.c:693
#, c-format
msgid "Invalid argument \"%d\" for --%s: the argument must be positive\n"
msgstr ""

#: ../libwnck/wnckprop.c:788
#, c-format
msgid "Conflicting options are present: --%s or --%s, and --%s\n"
msgstr ""

#: ../libwnck/wnckprop.c:820
#, c-format
msgid "Invalid argument \"%s\" for --%s, valid values are: %s\n"
msgstr ""

#: ../libwnck/wnckprop.c:863
#, c-format
msgid ""
"Cannot change the workspace layout on the screen: the layout is already "
"owned\n"
msgstr ""

#. Translators: 'viewport' is kind of the viewable area. A viewport
#. * can be used to implement workspaces (e.g. compiz is an example);
#. * however it is not just the current workspace.
#: ../libwnck/wnckprop.c:898
#, c-format
msgid ""
"Viewport cannot be moved: the current workspace does not contain a viewport\n"
msgstr ""

#. Translators: 'viewport' is kind of the viewable area. A viewport
#. * can be used to implement workspaces (e.g. compiz is an example);
#. * however it is not just the current workspace.
#: ../libwnck/wnckprop.c:905
#, c-format
msgid "Viewport cannot be moved: there is no current workspace\n"
msgstr ""

#. FIXME: why do we have dual & boolean API. This is not consistent!
#: ../libwnck/wnckprop.c:941 ../libwnck/wnckprop.c:950
#: ../libwnck/wnckprop.c:959 ../libwnck/wnckprop.c:966
#: ../libwnck/wnckprop.c:976 ../libwnck/wnckprop.c:983
#: ../libwnck/wnckprop.c:992 ../libwnck/wnckprop.c:1041
#, c-format
msgid "Action not allowed\n"
msgstr "Handling ikke tillatt\n"

#: ../libwnck/wnckprop.c:1037
#, c-format
msgid "Window cannot be moved to workspace %d: the workspace does not exist\n"
msgstr ""

#. Translators: 'unset' in the sense of "something has not been set".
#: ../libwnck/wnckprop.c:1101 ../libwnck/wnckprop.c:1233
msgid "<name unset>"
msgstr "<navn ikke satt>"

#. Translators: %lu is a window number and %s a window name
#: ../libwnck/wnckprop.c:1104
#, c-format
msgid "%lu: %s\n"
msgstr "%lu: %s\n"

#. Translators: %d is a workspace number and %s a workspace name
#: ../libwnck/wnckprop.c:1124
#, c-format
msgid "%d: %s\n"
msgstr "%d: %s\n"

#: ../libwnck/wnckprop.c:1187
#, c-format
msgid "Screen Number: %d\n"
msgstr "Skjermnummer: %d\n"

#: ../libwnck/wnckprop.c:1189 ../libwnck/wnckprop.c:1270
#, c-format
msgid "Geometry (width, height): %d, %d\n"
msgstr "Geometri (bredde, høyd): %d, %d\n"

#: ../libwnck/wnckprop.c:1193
#, c-format
msgid "Number of Workspaces: %d\n"
msgstr "Antall arbeidsområder: %d\n"

#: ../libwnck/wnckprop.c:1199
#, c-format
msgid "Workspace Layout (rows, columns, orientation): %d, %d, %s\n"
msgstr ""

#: ../libwnck/wnckprop.c:1209 ../libwnck/wnckprop.c:1266
#: ../libwnck/wnckprop.c:1460
msgid "<no EWMH-compliant window manager>"
msgstr ""

#: ../libwnck/wnckprop.c:1210
#, c-format
msgid "Window Manager: %s\n"
msgstr "Vindushåndterer: %s\n"

#. Translators: %d is a workspace number and %s a workspace name
#: ../libwnck/wnckprop.c:1215 ../libwnck/wnckprop.c:1296
#: ../libwnck/wnckprop.c:1308 ../libwnck/wnckprop.c:1320
#: ../libwnck/wnckprop.c:1332 ../libwnck/wnckprop.c:1445
#, c-format
msgid "%d (\"%s\")"
msgstr "%d («%s»)"

#. Translators: "none" here means "no workspace"
#: ../libwnck/wnckprop.c:1220 ../libwnck/wnckprop.c:1301
#: ../libwnck/wnckprop.c:1313 ../libwnck/wnckprop.c:1325
#: ../libwnck/wnckprop.c:1337 ../libwnck/wnckprop.c:1452
msgctxt "workspace"
msgid "none"
msgstr "ingen"

#: ../libwnck/wnckprop.c:1221
#, c-format
msgid "Active Workspace: %s\n"
msgstr "Aktivt arbeidsområde: %s\n"

#: ../libwnck/wnckprop.c:1230
#, c-format
msgid "\"%s\""
msgstr "«%s»"

#. Translators: %lu is a window identifier (number) and %s a window name
#: ../libwnck/wnckprop.c:1236
#, c-format
msgid "%lu (%s)"
msgstr "%lu (%s)"

#. Translators: "none" here means "no window"
#: ../libwnck/wnckprop.c:1242
msgctxt "window"
msgid "none"
msgstr "ingen"

#: ../libwnck/wnckprop.c:1243
#, c-format
msgid "Active Window: %s\n"
msgstr "Aktivt vindu: %s\n"

#: ../libwnck/wnckprop.c:1246
#, c-format
msgid "Showing the desktop: %s\n"
msgstr "Viser skrivebordet: %s\n"

#: ../libwnck/wnckprop.c:1248
msgid "true"
msgstr "sann"

#: ../libwnck/wnckprop.c:1248
msgid "false"
msgstr "usnn"

#: ../libwnck/wnckprop.c:1259
#, c-format
msgid "Workspace Name: %s\n"
msgstr "Navn på arbeidsområde: %s\n"

#: ../libwnck/wnckprop.c:1260
#, c-format
msgid "Workspace Number: %d\n"
msgstr "Arbeidsområde nummer: %d\n"

#: ../libwnck/wnckprop.c:1267 ../libwnck/wnckprop.c:1461
#, c-format
msgid "On Screen: %d (Window Manager: %s)\n"
msgstr "På skjerm: %d (Vindushåndterer: %s)\n"

#. Translators: 'viewport' is kind of the viewable area. A viewport can be
#. * used to implement workspaces (e.g. compiz is an example); however it is
#. * not just the current workspace.
#: ../libwnck/wnckprop.c:1282
msgid "<no viewport>"
msgstr "<ikke noe visningområde>"

#. Translators: 'viewport' is kind of the viewable area. A viewport can be
#. * used to implement workspaces (e.g. compiz is an example); however it is
#. * not just the current workspace.
#: ../libwnck/wnckprop.c:1286
#, c-format
msgid "Viewport position (x, y): %s\n"
msgstr ""

#: ../libwnck/wnckprop.c:1289
#, c-format
msgid "Position in Layout (row, column): %d, %d\n"
msgstr ""

#: ../libwnck/wnckprop.c:1302
#, c-format
msgid "Left Neighbor: %s\n"
msgstr "Venstre nabo: %s\n"

#: ../libwnck/wnckprop.c:1314
#, c-format
msgid "Right Neighbor: %s\n"
msgstr "Høyre nabo: %s\n"

#: ../libwnck/wnckprop.c:1326
#, c-format
msgid "Top Neighbor: %s\n"
msgstr ""

#: ../libwnck/wnckprop.c:1338
#, c-format
msgid "Bottom Neighbor: %s\n"
msgstr ""

#. Translators: Resource class is the name to identify a class.
#: ../libwnck/wnckprop.c:1350
#, c-format
msgid "Resource Class: %s\n"
msgstr "Ressursklasse: %s\n"

#: ../libwnck/wnckprop.c:1352
#, c-format
msgid "Group Name: %s\n"
msgstr "Gruppenavn: %s\n"

#. Translators: 'set' in the sense of "something has been set".
#: ../libwnck/wnckprop.c:1358 ../libwnck/wnckprop.c:1382
#: ../libwnck/wnckprop.c:1436
msgid "set"
msgstr "satt"

#. Translators: 'unset' in the sense of "something has not been set".
#: ../libwnck/wnckprop.c:1361 ../libwnck/wnckprop.c:1385
#: ../libwnck/wnckprop.c:1392 ../libwnck/wnckprop.c:1422
#: ../libwnck/wnckprop.c:1429 ../libwnck/wnckprop.c:1439
#: ../libwnck/wnckprop.c:1504 ../libwnck/wnckprop.c:1515
#: ../libwnck/wnckprop.c:1523
msgid "<unset>"
msgstr "<ikke satt>"

#: ../libwnck/wnckprop.c:1362 ../libwnck/wnckprop.c:1386
#: ../libwnck/wnckprop.c:1440
#, c-format
msgid "Icons: %s\n"
msgstr "Ikoner: %s\n"

#: ../libwnck/wnckprop.c:1365 ../libwnck/wnckprop.c:1403
#, c-format
msgid "Number of Windows: %d\n"
msgstr "Antall vinduer: %d\n"

#: ../libwnck/wnckprop.c:1377 ../libwnck/wnckprop.c:1423
#, c-format
msgid "Name: %s\n"
msgstr "Navn: %s\n"

#. Translators: note that "Icon" here has a specific window
#. * management-related meaning. It means minimized.
#: ../libwnck/wnckprop.c:1378 ../libwnck/wnckprop.c:1432
#, c-format
msgid "Icon Name: %s\n"
msgstr "Ikonnavn: %s\n"

#: ../libwnck/wnckprop.c:1393 ../libwnck/wnckprop.c:1516
#, c-format
msgid "PID: %s\n"
msgstr "PID: %s\n"

#. Translators: "none" here means "no startup ID"
#: ../libwnck/wnckprop.c:1400
msgctxt "startupID"
msgid "none"
msgstr "ingen"

#: ../libwnck/wnckprop.c:1401
#, c-format
msgid "Startup ID: %s\n"
msgstr "Oppstarts-ID: %s\n"

#: ../libwnck/wnckprop.c:1449
msgid "all workspaces"
msgstr "alle arbeidsområder"

#: ../libwnck/wnckprop.c:1453
#, c-format
msgid "On Workspace: %s\n"
msgstr "På arbeidsområde %s\n"

#: ../libwnck/wnckprop.c:1468
msgid "normal window"
msgstr "normalt vindu"

#: ../libwnck/wnckprop.c:1471
msgid "desktop"
msgstr "skrivebord"

#: ../libwnck/wnckprop.c:1474
msgid "dock or panel"
msgstr "dokk eller panel"

#: ../libwnck/wnckprop.c:1477
msgid "dialog window"
msgstr "dialogvindu"

#: ../libwnck/wnckprop.c:1480
msgid "tearoff toolbar"
msgstr "avtagbar verktøylinje"

#: ../libwnck/wnckprop.c:1483
msgid "tearoff menu"
msgstr "avtagbar meny"

#: ../libwnck/wnckprop.c:1486
msgid "utility window"
msgstr "verktøyvindu"

#: ../libwnck/wnckprop.c:1489
msgid "splash screen"
msgstr "oppstartsskjerm"

#: ../libwnck/wnckprop.c:1494
#, c-format
msgid "Window Type: %s\n"
msgstr "Type vindu: %s\n"

#: ../libwnck/wnckprop.c:1497
#, c-format
msgid "Geometry (x, y, width, height): %d, %d, %d, %d\n"
msgstr "Geometri (x, y, bredde, høyde): %d, %d, %d, %d\n"

#. Translators: A class is like a "family". E.g., all gvim windows are of the
#. * same class.
#: ../libwnck/wnckprop.c:1507
#, c-format
msgid "Class Group: %s\n"
msgstr "Klassegruppe: %s\n"

#: ../libwnck/wnckprop.c:1509
#, c-format
msgid "XID: %lu\n"
msgstr "XID: %lu\n"

#: ../libwnck/wnckprop.c:1524
#, c-format
msgid "Session ID: %s\n"
msgstr "Sesjons-ID: %s\n"

#. Translators: A group leader is the window that is the "owner" of a group
#. * of windows, ie: if you have multiple windows in one application, one
#. * window has some information about the application (like the application
#. * name).
#: ../libwnck/wnckprop.c:1531
#, c-format
msgid "Group Leader: %lu\n"
msgstr "Gruppeleder: %lu\n"

#. Translators: A window can be transient for another window: it means it's
#. * on top of it
#: ../libwnck/wnckprop.c:1537
#, c-format
msgid "Transient for: %lu\n"
msgstr "Transient for: %lu\n"

#. FIXME: else print something?
#. Translators: we're building a list of items here.             * For example, the result is "a, b".                            * In this case, the first string is "a", the second             * string is ", " and the third string is "b".                   * We can then use this information here to also                 * recursively build longer lists, like "a, b, c, d"
#. Translators: we're building a list of items here.             * The end result is something like "a, b, c"                    * In this case, the first string is "a, b", the second          * string is ", " and the third string is "c"
#: ../libwnck/wnckprop.c:1552 ../libwnck/wnckprop.c:1600
#, c-format
msgid "%1$s%2$s%3$s"
msgstr "%1$s%2$s%3$s"

#. Translators: see comment for "%1$s%2$s%3$s" in order          * to properly translate this
#: ../libwnck/wnckprop.c:1556 ../libwnck/wnckprop.c:1602
msgid ", "
msgstr ", "

#: ../libwnck/wnckprop.c:1562
msgid "minimized"
msgstr "minimert"

#: ../libwnck/wnckprop.c:1563
msgid "maximized"
msgstr "maksimert"

#: ../libwnck/wnckprop.c:1567
msgid "maximized horizontally"
msgstr "maksimert horisontalt"

#: ../libwnck/wnckprop.c:1569
msgid "maximized vertically"
msgstr "maksimert vertikalt"

#: ../libwnck/wnckprop.c:1571
msgid "shaded"
msgstr "rullet opp"

#: ../libwnck/wnckprop.c:1572
msgid "pinned"
msgstr "festet"

#: ../libwnck/wnckprop.c:1573
msgid "sticky"
msgstr "klebrig"

#: ../libwnck/wnckprop.c:1574
msgid "above"
msgstr "over"

#: ../libwnck/wnckprop.c:1575
msgid "below"
msgstr "under"

#: ../libwnck/wnckprop.c:1576
msgid "fullscreen"
msgstr "fullskjerm"

#: ../libwnck/wnckprop.c:1577
msgid "needs attention"
msgstr "trenger oppmerksomhet"

#. Translators: A pager is the technical term for the workspace switcher.
#. * It's a representation of all workspaces with windows inside it.
#. * Please make sure that the translation is in sync with gnome-panel,
#. * where this term is also used in translatable strings
#: ../libwnck/wnckprop.c:1582
msgid "skip pager"
msgstr "hopp over arbeidsområdebytter"

#. Translators: "tasklist" is the list of running applications (the window
#. * list)
#: ../libwnck/wnckprop.c:1585
msgid "skip tasklist"
msgstr "hopp over oppgaveliste"

#: ../libwnck/wnckprop.c:1587
msgid "normal"
msgstr "normal"

#: ../libwnck/wnckprop.c:1588
#, c-format
msgid "State: %s\n"
msgstr "Tilstand: %s\n"

#: ../libwnck/wnckprop.c:1609
msgid "move"
msgstr "flytt"

#: ../libwnck/wnckprop.c:1610
msgid "resize"
msgstr "endre størrelse"

#: ../libwnck/wnckprop.c:1611
msgid "shade"
msgstr "rull opp"

#: ../libwnck/wnckprop.c:1612
msgid "unshade"
msgstr "rull ned"

#: ../libwnck/wnckprop.c:1613
msgid "stick"
msgstr "fest"

#: ../libwnck/wnckprop.c:1614
msgid "unstick"
msgstr "løsne"

#: ../libwnck/wnckprop.c:1616
msgid "maximize horizontally"
msgstr "maksimer horisontalt"

#: ../libwnck/wnckprop.c:1618
msgid "unmaximize horizontally"
msgstr "gjenopprett horisontalt"

#: ../libwnck/wnckprop.c:1620
msgid "maximize vertically"
msgstr "maksimer vertikalt"

#: ../libwnck/wnckprop.c:1622
msgid "unmaximize vertically"
msgstr "gjenopprett vertikalt"

#: ../libwnck/wnckprop.c:1625
msgid "change workspace"
msgstr "endre arbeidsområde"

#: ../libwnck/wnckprop.c:1627
msgid "pin"
msgstr "fest"

#: ../libwnck/wnckprop.c:1629
msgid "unpin"
msgstr "løsne"

#: ../libwnck/wnckprop.c:1630
msgid "minimize"
msgstr "minimer"

#: ../libwnck/wnckprop.c:1631
msgid "unminimize"
msgstr "gjenopprett"

#: ../libwnck/wnckprop.c:1632
msgid "maximize"
msgstr "maksimer"

#: ../libwnck/wnckprop.c:1633
msgid "unmaximize"
msgstr "gjenopprett"

#: ../libwnck/wnckprop.c:1635
msgid "change fullscreen mode"
msgstr "endre fullskjermmodus"

#: ../libwnck/wnckprop.c:1636
msgid "close"
msgstr "lukk"

#: ../libwnck/wnckprop.c:1638
msgid "make above"
msgstr "plasser over"

#: ../libwnck/wnckprop.c:1640
msgid "unmake above"
msgstr "ikke plasser over"

#: ../libwnck/wnckprop.c:1642
msgid "make below"
msgstr "plasser under"

#: ../libwnck/wnckprop.c:1644
msgid "unmake below"
msgstr "ikke plasser under"

#: ../libwnck/wnckprop.c:1646
msgid "no action possible"
msgstr "ingen handling mulig"

#: ../libwnck/wnckprop.c:1647
#, c-format
msgid "Possible Actions: %s\n"
msgstr "Mulige handlinger: %s\n"

#: ../libwnck/wnckprop.c:1826
msgid ""
"Print or modify the properties of a screen/workspace/window, or interact "
"with it, following the EWMH specification.\n"
"For information about this specification, see:\n"
"\thttp://freedesktop.org/wiki/Specifications/wm-spec"
msgstr ""

#: ../libwnck/wnckprop.c:1836
msgid "Options to list windows or workspaces"
msgstr "Alternativer for visning av vinduer eller arbeidsområder"

#: ../libwnck/wnckprop.c:1837
msgid "Show options to list windows or workspaces"
msgstr "Vis alternativ for å vise vinduer eller arbeidsområder"

#: ../libwnck/wnckprop.c:1844
msgid "Options to modify properties of a window"
msgstr "Alternativer for å endre egenskaper for et vindu"

#: ../libwnck/wnckprop.c:1845
msgid "Show options to modify properties of a window"
msgstr "Vis alternativer for å endre egenskaper for et vindu"

#: ../libwnck/wnckprop.c:1852
msgid "Options to modify properties of a workspace"
msgstr "Alternativer for å endre egenskaper for et arbeidsområde"

#: ../libwnck/wnckprop.c:1853
msgid "Show options to modify properties of a workspace"
msgstr "Vis alternativer for å endre egenskaper for et arbeidsområde"

#: ../libwnck/wnckprop.c:1860
msgid "Options to modify properties of a screen"
msgstr "Alternativer for å endre egenskaper for en skjerm"

#: ../libwnck/wnckprop.c:1861
msgid "Show options to modify properties of a screen"
msgstr "Vis alternativer for å endre egenskaper for en skjerm"

#: ../libwnck/wnckprop.c:1872
#, c-format
msgid "Error while parsing arguments: %s\n"
msgstr "Feil under lesing av argumenter: %s\n"

#: ../libwnck/wnckprop.c:1895
#, c-format
msgid "Cannot interact with screen %d: the screen does not exist\n"
msgstr "Kan ikke kommunisere med skjerm %d: skjermen eksisterer ikke\n"

#: ../libwnck/wnckprop.c:1951
#, c-format
msgid "Cannot interact with workspace %d: the workspace cannot be found\n"
msgstr ""
"Kan ikke kommunisere med arbeidsområde %d: arbeidsområdet finnes ikke\n"

#. Translators: A class is like a "family". E.g., all gvim windows are
#. * of the same class.
#: ../libwnck/wnckprop.c:1975
#, c-format
msgid ""
"Cannot interact with class group \"%s\": the class group cannot be found\n"
msgstr ""
"Kan ikke ha interaksjon med klassegruppen «%s»: klassegruppen ble ikke "
"funnet\n"

#: ../libwnck/wnckprop.c:1998
#, c-format
msgid ""
"Cannot interact with application having its group leader with XID %lu: the "
"application cannot be found\n"
msgstr ""

#: ../libwnck/wnckprop.c:2021
#, c-format
msgid "Cannot interact with window with XID %lu: the window cannot be found\n"
msgstr ""
"Kan ikke kommunisere med vindu som har XID %lu: vinduet ble ikke funnet\n"
